#include "CreditsScene.h"

#include <iostream>
using namespace std;


#define COCOS_DEBUG 1

USING_NS_CC;

Scene* CreditsScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
	auto layer = CreditsScene::create();

    // add layer as a child to scene
    scene->addChild(layer);




    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool CreditsScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    return true;
}
