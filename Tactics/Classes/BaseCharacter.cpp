#include "BaseCharacter.h"
#include <limits>
#include <cmath> 

BaseCharacter::BaseCharacter(int teamValue, int typeValue)
{
	team = teamValue;
	type = typeValue;
	
	isAlive = true;

	moveCount = -1;
	myIndex = -1;
	row = -1;
	coll = -1;

	nodePath = nullptr;

	SetMovementByType(type);
}

BaseCharacter::~BaseCharacter()
{
	//To Do
	/*
	if (nodePath)
	{
		delete nodePath;
	}
	*/
}

void BaseCharacter::SetIsAlive(bool value)
{
	isAlive = value;
}

bool BaseCharacter::GetIsAlive()
{
	return isAlive;
}

void BaseCharacter::StartTurn()
{
	moveCount = RandomHelper::random_int(3, maxMove);
}

void BaseCharacter::SetMovementByType(int typeValue)
{

	maxMove = 6;
	/*switch (typeValue)
	{
		case KNIGHT_TILE:
			maxMove = 3;
			break;
	
		case ARCHER_TILE:
			maxMove = 3;
			break;

		case ASSASSIN_TILE:
			maxMove = 3;
			break;

		case HEALER_TILE:
			maxMove = 3;
			break;

		default:
			maxMove = -1;
			CCLOG("Invalid character type! Can't set the maxMove variable");
				break;
	}*/
}

void BaseCharacter::SetNodePath(vector<TacticsTile *>* path)
{
	nodePath = path;
}

TacticsTile* BaseCharacter::GetTileToMove()
{
	if (nodePath->size() == 0 || moveCount <= 0)
		return nullptr;
	else
	{
		moveCount--;
		TacticsTile* tileToMove = (*nodePath)[nodePath->size() - 1];
		nodePath->pop_back();
		return tileToMove;
	}
}


BaseCharacter* BaseCharacter::SearchForEnemy(vector<BaseCharacter *> *charactersList)
{	
	unsigned int distance = numeric_limits<unsigned int>::max(); // Setting the uint to the max number possible
	BaseCharacter *enemy = nullptr;

	for (unsigned int i = 0; i < charactersList->size(); i++)
	{
		BaseCharacter *tempChar = (*charactersList)[i];

		if (tempChar->GetTeam() != team && tempChar->GetIsAlive())
		{
			int rowDistance = row - tempChar->GetRow();
			rowDistance = abs(rowDistance);

			int collDistance = coll - tempChar->GetColl();
			collDistance = abs(collDistance);

			if (distance > (rowDistance + collDistance))
			{
				enemy = tempChar;
				distance = rowDistance + collDistance;
			}
		}
	}
	
	return enemy;
}

int BaseCharacter::GetRow()
{
	return row;
}
int BaseCharacter::GetColl()
{
	return coll;
}

int BaseCharacter::GetTeam()
{
	return team;
}

void BaseCharacter::SetIndex(int value)
{
	myIndex = value;
}

int BaseCharacter::GetIndex()
{
	return myIndex;
}

void BaseCharacter::SetPosition(int rowValue, int collValue)
{
	row = rowValue;
	coll = collValue;
}

void BaseCharacter::SetRow(int value)
{
	row = value;
}

void BaseCharacter::SetColl(int value)
{
	coll = value;
}