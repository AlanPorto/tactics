#ifndef __TEAM_B_SCENE_H__
#define __TEAM_B_SCENE_H__

#include "cocos2d.h"

class TeamBScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // implement the "static create()" method manually
	CREATE_FUNC(TeamBScene);

private:
	void GoToMenuScene( float deltaTime);

};

#endif // __TEAM_B_SCENE_H__
