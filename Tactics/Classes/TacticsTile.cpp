#include "TacticsTile.h"

TacticsTile::TacticsTile()
{
	isOpen = false;

	tileType = -1;


	//tileNode = nullptr;

	testLabel = nullptr;

	emptySprite = Sprite::create("empty-tile.png");
	spriteList.push_back(emptySprite);

	wallSprite = Sprite::create("wall-tile.png");
	spriteList.push_back(wallSprite);

	teamA_Sprite = Sprite::create("teamA-tile.png");
	spriteList.push_back(teamA_Sprite);

	teamB_Sprite = Sprite::create("teamB-tile.png");
	spriteList.push_back(teamB_Sprite);

	knightSprite = Sprite::create("knight-tile.png");
	spriteList.push_back(knightSprite);

	archerSprite = Sprite::create("archer-tile.png");
	spriteList.push_back(archerSprite);

	healerSprite = Sprite::create("healer-tile.png");
	spriteList.push_back(healerSprite);

	assassinSprite = Sprite::create("assassin-tile.png");
	spriteList.push_back(assassinSprite);

	// --- Mouse Event
	mouseEvent = EventListenerMouse::create();
	mouseEvent->onMouseDown = CC_CALLBACK_1(TacticsTile::HandleClick, this);

	emptySprite->getEventDispatcher()->addEventListenerWithFixedPriority(mouseEvent, 1);
}

void TacticsTile::HandleClick(Event* event)
{
	/*tileNode;
	TacticsNode *debug = tileNode->GetParentNode();
	debug;*/
	CCLOG("Break");
}

TacticsTile::~TacticsTile()
{
	/*
	if (tileNode != NULL)
		delete tileNode;
	*/
}

void TacticsTile::InitTile(Layer *refLayer, int row, int col)
{
	parent = refLayer;
	
	tileRow = row;
	tileColl = col;
	

	auto visibleSize = Director::getInstance()->getVisibleSize();
	auto origin = Director::getInstance()->getVisibleOrigin();

	float tileWidth = emptySprite->getContentSize().width;
	float tileheight = emptySprite->getContentSize().height;

	float offSetX = 72;
	float offSetY = visibleSize.height - 20;

	float posX = offSetX + (tileWidth * col);
	float posY = offSetY - (tileWidth * row);

	SetSpritesPosition(posX, posY);

	//Adding sprites to the scene
	for (unsigned i = 0; i < spriteList.size(); i++)
	{
		parent->addChild(spriteList[i]);
	}

	HideAllTiles();
}

void TacticsTile::SetSpritesPosition(float posX, float posY)
{
	for (unsigned i = 0; i < spriteList.size(); i++)
	{
		spriteList[i]->setPosition(Point(posX, posY));
	}
}

void TacticsTile::UpdateTileType(int value)
{
		tileType = value;
		HideAllTiles();

		switch (value)
		{
			case EMPTY_TILE:
				emptySprite->setVisible(true);
				isOpen = true;
				break;

			case WALL_TILE:
				wallSprite->setVisible(true);
				isOpen = false;
				break;

			case KNIGHT_TILE:
				knightSprite->setVisible(true);
				isOpen = false;
				break;

			case ARCHER_TILE:
				archerSprite->setVisible(true);
				isOpen = false;
				break;

			case ASSASSIN_TILE:
				assassinSprite->setVisible(true);
				isOpen = false;
				break;

			default:
				break;
		}
}

void TacticsTile::SetDebugText(int value)
{
	
	if (value == TEAM_A_TILE)
	{
		HideAllTiles();
		teamA_Sprite->setVisible(true);
	}
	else if (value == TEAM_B_TILE)
	{
		HideAllTiles();
		teamB_Sprite->setVisible(true);
	}
	else
	{
		UpdateTileType(tileType);
	}

	/*
	if (testLabel != nullptr)
	{
		parent->removeChild(testLabel);
		testLabel = nullptr;
	}

	if (value > 0)
	{
		string valueString = to_string(value);
		testLabel = Label::createWithTTF(valueString, "fonts/arial.ttf", 8);
	}
	else
		testLabel = Label::createWithTTF("", "fonts/arial.ttf", 8);

	testLabel->setColor(Color3B::BLACK);

	testLabel->setAnchorPoint(emptySprite->getAnchorPoint());
	testLabel->setPosition(emptySprite->getPosition());
	parent->addChild(testLabel);
	*/
	// ----
}

void TacticsTile::HideAllTiles()
{
	for (unsigned i = 0; i < spriteList.size(); i++)
	{
		Sprite *tempSprite = spriteList[i];
		
		if (tempSprite->isVisible())
			tempSprite->setVisible(false);
	}
}

//---- Path Find Node
int TacticsTile::GetRow() { return tileRow; }
int TacticsTile::GetColl() { return tileColl; }
int TacticsTile::GetType() { return tileType; }


int TacticsTile::Get_G_Cost(){ return g_cost; }
int TacticsTile::Get_H_Cost(){ return h_cost; }
int TacticsTile::Get_F_Cost(){ return f_cost; }


bool TacticsTile::IsOpen() { return isOpen; }
void TacticsTile::OpenNode() { isOpen = true; }

void TacticsTile::Set_G_Cost(int newCost)
{
	g_cost = newCost;
	Set_F_Cost();
}

void TacticsTile::Set_H_Cost(int newCost)
{
	h_cost = newCost;
	Set_F_Cost();
}

void TacticsTile::Set_F_Cost()
{
	f_cost = g_cost + h_cost;
}


TacticsTile* TacticsTile::GetParentNode()
{
	return parentNode;
}

void TacticsTile::SetParentNode(TacticsTile* node)
{
	parentNode = node;
}

void TacticsTile::ResetNode()
{
	parentNode = nullptr;

	f_cost = -1;
	g_cost = -1;
	h_cost = -1;

	UpdateTileType(tileType);

	isOpen = (tileType == EMPTY_TILE);
}