#include "MenuScene.h"
#include "GameScene.h"
#include "CreditsScene.h"
#include "Definitions.h"

#define COCOS_DEBUG 1

USING_NS_CC;

Scene* MenuScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
	auto layer = MenuScene::create();

    // add layer as a child to scene
    scene->addChild(layer);
	
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool MenuScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto backgroundSprite = Sprite::create("HelloWorld.png");

	backgroundSprite->setPosition(Point(visibleSize.width * 0.5f + origin.x, visibleSize.height * 0.5f + origin.y));
	this->addChild(backgroundSprite);

	auto playBtn = MenuItemImage::create("btn_play_normal.png", "btn_play_down.png", CC_CALLBACK_1(MenuScene::GoToGameScene, this));
	playBtn->setPosition(Point(visibleSize.width * 0.5f + origin.x, visibleSize.height * 0.3f + origin.y));

	auto menu = Menu::create(playBtn, NULL);
	menu->setPosition(Point::ZERO);

	this->addChild(menu);

    return true;
}

void MenuScene::GoToCreditsScene(Ref *sender)
{
	auto scene = CreditsScene::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
}

void MenuScene::GoToGameScene(Ref *sender)
{
	auto scene = GameScene::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
}