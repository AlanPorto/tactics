#ifndef __TACTICS_TILE_H__
#define __TACTICS_TILE_H__

#include "Definitions.h"
#include <vector>


#include "cocos2d.h"
USING_NS_CC;
using namespace std;

class TacticsTile
{
public:
	// --- Variables


	// --- Methods
	TacticsTile();
	~TacticsTile();

	void InitTile(Layer *refLayer, int row, int col);
	void UpdateTileType(int value);
	void ShowTile(int tileType);
	void OpenNode();
	bool IsOpen();
	int GetRow();
	int GetColl();
	int GetType();

	void SetDebugText(int value);
	
	//----- Node

	int Get_G_Cost();
	int Get_H_Cost();
	int Get_F_Cost();

	void Set_G_Cost(int newCost);
	void Set_H_Cost(int newCost);

	void ResetNode();
	void SetParentNode(TacticsTile* node);
	TacticsTile* GetParentNode();


private:
	// --- Debug
	EventListenerMouse* mouseEvent;

	void HandleClick(Event* event);
	
	Label* testLabel;


	// --- Variables
	bool isOpen;
	int tileType;
	int tileRow;
	int tileColl;

	//TacticsNode *tileNode;
	
	Layer *parent;

	Sprite *emptySprite;
	Sprite *wallSprite;
	Sprite *teamA_Sprite;
	Sprite *teamB_Sprite;

	Sprite *knightSprite;
	Sprite *archerSprite;
	Sprite *healerSprite;
	Sprite *assassinSprite;

	vector<Sprite*> spriteList;

	// --- Methods
	void SetSpritesPosition(float posX, float posY);
	void HideAllTiles();


	// --- Private node
	int row;
	int coll;

	int f_cost;
	int g_cost;
	int h_cost;

	// --- Methods
	TacticsTile* parentNode;
	void Set_F_Cost();


};

#endif // __TACTICS_TILE_H__

