#include "GameScene.h"
#include "Definitions.h"
#include "TeamAScene.h"
#include "TeamBScene.h"
#include "SimpleAudioEngine.h"


USING_NS_CC;

Scene* GameScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = GameScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool GameScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	// ---------------------------------

	auto audio = CocosDenshion::SimpleAudioEngine::getInstance();

	if (audio->isBackgroundMusicPlaying() == false)
	{
		audio->playBackgroundMusic("Sounds/Background.mp3", true);
	}

	ConstructGameScene();
	CreateTeams();
	CreateLevel();


	// --- Keyboard Event
	keyboardEvent = EventListenerKeyboard::create();
	keyboardEvent->onKeyPressed = CC_CALLBACK_2(GameScene::HandleKeyPress, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(keyboardEvent, this);

	// Scheduling update
	//this->scheduleUpdate();
	this->scheduleUpdate();

	return true;
}

void GameScene::ConstructGameScene()
{
	pathFound = false;
	teamATurn = false;
	
	victoriousTeam = -1;
	currentTurnIndex = -1;
	timer = 0.0f;

	currentChar = nullptr;
	currentEnemy = nullptr;
	currentTile = nullptr;
	targetTile = nullptr;
}

void GameScene::DestroyGameScene()
{
	for (vector<TacticsTile*>::iterator i = nodePath.begin(); i != nodePath.end(); i++)
	{
		delete (*i);
	}
	nodePath.clear();


	for (vector<BaseCharacter *>::iterator i = charactersList.begin(); i != charactersList.end(); i++)
	{
		delete (*i);
	}
	charactersList.clear();

	for (int i = 0; i < TILE_LIST_SIZE; i++)
	{
		TacticsTile *tile = &tileList[i];

		if (tile)
			delete tile;
	}

	if (currentChar)
		delete currentChar;

	if (currentEnemy)
		delete currentEnemy;
	
	if (currentTile)
		delete currentTile;

	if (targetTile)
		delete targetTile;

	if (keyboardEvent)
		delete keyboardEvent;
}

void GameScene::update(float delta)
{
	if (pathFound)
	{
		float timeTick = 0.55f;
		timer += delta;

		//Move the currentTile when reach the tick time
		if (timer >= timeTick) 
		{
			timer = 0.0f;

			TacticsTile *characterTile = GetTileByGridPosition(currentChar->GetRow(), currentChar->GetColl());
			TacticsTile *tileToMove = currentChar->GetTileToMove();

			if (tileToMove == nullptr) //If the character finished his path
			{
				CheckAttack();
			}
			else
			{
				int direction = GetDirectionToTile(characterTile, tileToMove);

				if (direction <= -1)
					CCLOG("Error on getting the direction");
				else
				{
					MoveTile(characterTile, direction);

					currentChar->SetRow(tileToMove->GetRow());
					currentChar->SetColl(tileToMove->GetColl());

					auto audio = CocosDenshion::SimpleAudioEngine::getInstance();

					if (currentChar->GetTeam() == TEAM_A)
						audio->playEffect("Sounds/step02.mp3", false, 1.0f, 1.0f, 1.0f);
					else
						audio->playEffect("Sounds/step01.mp3", false, 1.0f, 1.0f, 1.0f);
					
				}
			}
		}
	}
}

void GameScene::CheckAttack()
{
	
	int rowDistance =  abs(currentChar->GetRow() - currentEnemy->GetRow());
	int collDistance = abs(currentChar->GetColl() - currentEnemy->GetColl());

	if ((rowDistance == 1 && collDistance == 0) || (rowDistance == 0 && collDistance == 1))
	{
		auto audio = CocosDenshion::SimpleAudioEngine::getInstance();

		int rndNumber = RandomHelper::random_int(0, 100);
		
		if (rndNumber > 40)
		{
			currentEnemy->SetIsAlive(false);
			SetMapValue(currentEnemy->GetRow(), currentEnemy->GetColl(), EMPTY_TILE);

			if (currentChar->GetTeam() == TEAM_A)
				audio->playEffect("Sounds/atk01.mp3", false, 1.0f, 1.0f, 1.0f);
			else
				audio->playEffect("Sounds/atk02.mp3", false, 1.0f, 1.0f, 1.0f);
		}
		else
		{
			currentChar->SetIsAlive(false);
			SetMapValue(currentChar->GetRow(), currentChar->GetColl(), EMPTY_TILE);
			audio->playEffect("Sounds/atk01.mp3", false, 1.0f, 1.0f, 1.0f);


			if (currentEnemy->GetTeam() == TEAM_A)
				audio->playEffect("Sounds/atk01.mp3", false, 1.0f, 1.0f, 1.0f);
			else
				audio->playEffect("Sounds/atk02.mp3", false, 1.0f, 1.0f, 1.0f);
		}
	}

	if (IsGameOver())
	{
		MoveToEndScene();
	}
	else
	{
		pathFound = false;
		nodePath.clear();

		this->scheduleOnce(schedule_selector(GameScene::NextTurn), TIME_BETWEEN_TURNS);
	}
}

bool GameScene::IsGameOver()
{
	bool teamACanFight = false;
	bool teamBCanFight = false;

	for (int i = 0; i < charactersList.size(); i++)
	{
		BaseCharacter *charRef = charactersList[i];

		if (charRef->GetTeam() == TEAM_A && charRef->GetIsAlive())
		{
			teamACanFight = true;
			victoriousTeam = TEAM_A;
		}
		else if (charRef->GetTeam() == TEAM_B && charRef->GetIsAlive())
		{
			teamBCanFight = true;
			victoriousTeam = TEAM_B;
		}

		if (teamACanFight && teamBCanFight)
			return false;
	}



	return true;

}

void GameScene::MoveToEndScene()
{
	Scene *endScene = nullptr;
	
	if (victoriousTeam == TEAM_A)
	{
		endScene = TeamAScene::createScene();
	}
	else
	{
		endScene = TeamBScene::createScene();
	}

	//DestroyGameScene();
	Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, endScene));
}

int GameScene::GetDirectionToTile(TacticsTile *tile, TacticsTile *target)
{
	int tileRow = tile->GetRow();
	int tileColl = tile->GetColl();
	
	int targetRow = target->GetRow();
	int targetColl = target->GetColl();

	if (tileColl > targetColl)
		return MOVE_LEFT;
	else if (tileColl < targetColl)
		return MOVE_RIGHT;
	else if (tileRow > targetRow)
		return MOVE_UP;
	else if (tileRow < targetRow)
		return MOVE_DOWN;
	else
		return -1;
}

void GameScene::HandleKeyPress(EventKeyboard::KeyCode keyCode, Event* event)
{
	switch (keyCode)
	{
	/*
	case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
			currentTile = MoveTile(currentTile, MOVE_LEFT);
			break;

		case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
			currentTile = MoveTile(currentTile, MOVE_RIGHT);
			break;

		case EventKeyboard::KeyCode::KEY_UP_ARROW:
			currentTile = MoveTile(currentTile, MOVE_UP);
			break;

		case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
			currentTile = MoveTile(currentTile, MOVE_DOWN);
			break;
	
			//------------
		case EventKeyboard::KeyCode::KEY_A:
			targetTile = MoveTile(targetTile, MOVE_LEFT);
			break;

		case EventKeyboard::KeyCode::KEY_D:
			targetTile = MoveTile(targetTile, MOVE_RIGHT);
			break;

		case EventKeyboard::KeyCode::KEY_W:
			targetTile = MoveTile(targetTile, MOVE_UP);
			break;

		case EventKeyboard::KeyCode::KEY_S:
			targetTile = MoveTile(targetTile, MOVE_DOWN);
			break;
		*/

		//---
		case EventKeyboard::KeyCode::KEY_SPACE:
			NextTurn(0);
			break;
	}
}

void GameScene::NextTurn(float deltaTime)
{	
	teamATurn = !teamATurn;

	if (teamATurn)
	{
		currentTurnIndex++;

		if (currentTurnIndex >= TEAM_SIZE)
			currentTurnIndex = 0;
		
		currentChar = GetCharacterByIndex(TEAM_A, currentTurnIndex);
	}
	else
	{
		currentChar = GetCharacterByIndex(TEAM_B, currentTurnIndex);
	}

	if (currentChar->GetIsAlive() == false)
	{
		NextTurn(0);
		return;
	}

	currentChar->StartTurn();
	currentTile = GetTileByGridPosition(currentChar->GetRow(), currentChar->GetColl());
	//BaseCharacter *enemy = currentChar->SearchForEnemy(&charactersList);
	currentEnemy = currentChar->SearchForEnemy(&charactersList);
	targetTile = GetTileByGridPosition(currentEnemy->GetRow(), currentEnemy->GetColl());

	MoveToTarget();
}

void GameScene::CreateTeams()
{	
	int tempTeamA[TEAM_SIZE] =
	{
		KNIGHT_TILE,
		KNIGHT_TILE,
		KNIGHT_TILE,
		KNIGHT_TILE,
		KNIGHT_TILE
	};


	int tempTeamB[TEAM_SIZE] =
	{
		ASSASSIN_TILE,
		ASSASSIN_TILE,
		ASSASSIN_TILE,
		ASSASSIN_TILE,
		ASSASSIN_TILE
	};

	for (unsigned int i = 0; i < TEAM_SIZE; i++)
	{
		teamA[i] = tempTeamA[i];
		teamB[i] = tempTeamB[i];
	}
}

void GameScene::CreateLevel()
{
	int level[GRID_SIZE_ROW][GRID_SIZE_COLL] =
	{
		{ 1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1 },
		{ 1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1 },
		{ 1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1 },
		{ 1, -1,  0,  0,  0,  0,  1,  1,  0,  0,  0,  0,  0, -2,  1 },
		{ 1,  1, -1,  0,  0,  0,  1,  1,  0,  0,  0,  0, -2,  1,  1 },
		{ 1,  1,  1, -1,  0,  0,  1,  1,  0,  0,  0, -2,  1,  1,  1 },
		{ 1,  1, -1,  0,  0,  0,  1,  1,  0,  0,  0,  0, -2,  1,  1 },
		{ 1, -1,  0,  0,  0,  0,  1,  1,  0,  0,  0,  0,  0, -2,  1 },
		{ 1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1 },
		{ 1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1 },
		{ 1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1 }
	};

	int index = 0;
	int teamA_index = 0;
	int teamB_index = 0;
	
	for (int i = 0; i < GRID_SIZE_ROW; i++)
	{
		for (int j = 0; j < GRID_SIZE_COLL; j++)
		{
			TacticsTile tile;
			tile.InitTile(this, i, j);
			tileList[index] = tile;

			if (level[i][j] == TEAM_A)
			{
				int characterType = teamA[teamA_index];

				level[i][j] = characterType;

				BaseCharacter *character = new BaseCharacter(TEAM_A, characterType);
				character->SetPosition(i, j);
				character->SetIndex(teamA_index);
				charactersList.push_back(character);

				teamA_index++;
			}
			else if (level[i][j] == TEAM_B)
			{
				int characterType = teamB[teamB_index];

				level[i][j] = characterType;

				BaseCharacter *character = new BaseCharacter(TEAM_B, characterType);
				character->SetPosition(i, j);
				character->SetIndex(teamB_index);
				charactersList.push_back(character);

				teamB_index++;
			}

			SetMapValue(i, j, level[i][j]);
			index++;
		}
	}
}

void GameScene::SetMapValue(int row, int col, int value)
{
	grid[row][col] = value;

	TacticsTile *tempTile = GetTileByGridPosition(row, col);
	tempTile->UpdateTileType(value);
}

	
TacticsTile* GameScene::GetTileByGridPosition(int row, int col)
{
	for (int i = 0; i < TILE_LIST_SIZE; i++)
	{
		TacticsTile *tile = &tileList[i];

		if (tile->GetRow() == row && tile->GetColl() == col)
			return tile;
	}

	return nullptr;
}


BaseCharacter* GameScene::GetCharacterByIndex(int team, int index)
{
	for (unsigned int i = 0; i < charactersList.size(); i++)
	{
		BaseCharacter *tempChar = charactersList[i];

		if (tempChar->GetTeam() == team && tempChar->GetIndex() == index)
			return tempChar;
	}

	return nullptr;
}

BaseCharacter* GameScene::GetCharacterByPosition(int row, int coll)
{
	for (unsigned int i = 0; i < charactersList.size(); i++)
	{
		BaseCharacter *tempChar = charactersList[i];

		if (tempChar->GetRow() == row && tempChar->GetColl() == coll)
			return tempChar;
	}

	return nullptr;
}


void GameScene::MoveToTarget()
{
	if (!pathFound)
	{
		pathFound = StartPathFind(currentTile, targetTile);

		if (pathFound)
			MoveToTarget();
		else //Can't find a path
		{
			NextTurn(0);
		}
	}
	else
	{
		//Follow Path
		for (unsigned i = 0; i < nodePath.size(); i++)
		{
			TacticsTile* tempNode = nodePath[i];
			

			//teamATurn

			if (teamATurn)
				tempNode->SetDebugText(TEAM_A_TILE);
			else
				tempNode->SetDebugText(TEAM_B_TILE);
		}

		currentChar->SetNodePath(&nodePath);
	}
}

bool GameScene::CanAddNodeToList(TacticsTile *node, vector<TacticsTile*>* checkList)
{
	if (!node)
		return false;

	// Check if the node is inside the check list.
	for (unsigned i = 0; i < checkList->size(); i++)
	{
		TacticsTile *tempNode = (*checkList)[i];
		if (node == tempNode)
			return false;
	}

	return true;
}


bool GameScene::StartPathFind(TacticsTile *startNode, TacticsTile *endNode)
{	
	ResetNodes();

	startNode->OpenNode();
	endNode->OpenNode();

	endNode->Set_H_Cost(0);

	SetHeuristicCost(endNode);

	vector<TacticsTile*> openNodeList;
	vector<TacticsTile*> closedNodeList;

	// --- Create Path
	closedNodeList.push_back(startNode);

	startNode->Set_G_Cost(0);

	int g_costValue = 10;
	int lowest_f_cost = -1;
	
	TacticsTile *currentNode = startNode;

	while (true)
	{		
		if (currentNode == nullptr)
			return false;
		
		//Get the nodes around the currentNode
		TacticsTile *upNode = GetTileByGridPosition(currentNode->GetRow() - 1, currentNode->GetColl());
		TacticsTile *downNode = GetTileByGridPosition(currentNode->GetRow() + 1, currentNode->GetColl());
		TacticsTile *leftNode = GetTileByGridPosition(currentNode->GetRow(), currentNode->GetColl() - 1);
		TacticsTile *rightNode = GetTileByGridPosition(currentNode->GetRow(), currentNode->GetColl() + 1);

		
		//if (upNode && upNode->GetType() == WALL_TILE)
		if (upNode && !upNode->IsOpen())
			upNode = nullptr;

		//if (downNode && downNode->GetType() == WALL_TILE)
		if (downNode && !downNode->IsOpen())
			downNode = nullptr;

		
		//if (leftNode && leftNode->GetType() == WALL_TILE)
		if (leftNode && !leftNode->IsOpen())
			leftNode = nullptr;

		
		//if (rightNode && rightNode->GetType() == WALL_TILE)
		if (rightNode && !rightNode->IsOpen())
			rightNode = nullptr;


		//Add the nodes to the open list in case they aren't there on in the closed list
		if (CanAddNodeToList(upNode, &openNodeList) && CanAddNodeToList(upNode, &closedNodeList))
		{
			openNodeList.push_back(upNode);

			upNode->Set_G_Cost(currentNode->Get_G_Cost() + g_costValue);
			upNode->SetParentNode(currentNode);
		}

		if (CanAddNodeToList(downNode, &openNodeList) && CanAddNodeToList(downNode, &closedNodeList))
		{
			openNodeList.push_back(downNode);

			downNode->Set_G_Cost(currentNode->Get_G_Cost() + g_costValue);
			downNode->SetParentNode(currentNode);
		}

		if (CanAddNodeToList(leftNode, &openNodeList) && CanAddNodeToList(leftNode, &closedNodeList))
		{
			openNodeList.push_back(leftNode);

			leftNode->Set_G_Cost(currentNode->Get_G_Cost() + g_costValue);
			leftNode->SetParentNode(currentNode);
		}

		if (CanAddNodeToList(rightNode, &openNodeList) && CanAddNodeToList(rightNode, &closedNodeList))
		{
			openNodeList.push_back(rightNode);

			rightNode->Set_G_Cost(currentNode->Get_G_Cost() + g_costValue);
			rightNode->SetParentNode(currentNode);
		}


		//Check if one of the new nodes is the end node
		if (upNode && upNode == endNode)
			break;
		else if (downNode && downNode == endNode)
			break;
		else if (leftNode && leftNode == endNode)
			break;
		else if (rightNode && rightNode == endNode)
			break;

		//Check the F_Cost of the nodes around the currentNode
		TacticsTile* newCurNode = GetNodeWithLowFCost(lowest_f_cost, &openNodeList);

		openNodeList.erase(remove(openNodeList.begin(), openNodeList.end(), newCurNode), openNodeList.end());
		closedNodeList.push_back(newCurNode);
		currentNode = newCurNode;
		
	}

	CreatePath(endNode);
	return true;
	
}

TacticsTile* GameScene::GetNodeWithLowFCost(int fCost, vector<TacticsTile*>* checkList)
{
	TacticsTile* returnNode = nullptr;
	
	for (unsigned i = 0; i < checkList->size(); i++)
	{
		TacticsTile* node = (*checkList)[i];

		if (node->Get_F_Cost() < fCost || fCost < 0)
		{
			fCost = node->Get_F_Cost();
			returnNode = node;
		}
	}

	return returnNode;
}

void GameScene::CreatePath(TacticsTile *endNode)
{
	if (nodePath.size() > 0)
	{
		nodePath.clear();
	}

	nodePath.push_back(endNode);

	TacticsTile *lastNodeAdded = endNode;

	//Keep adding nodes until it doesn't have any parents
	while (lastNodeAdded->GetParentNode() != nullptr)
	{
		TacticsTile *parentNode = lastNodeAdded->GetParentNode();
		nodePath.push_back(parentNode);

		//Update the last node
		lastNodeAdded = parentNode;
	}

	nodePath.erase(nodePath.begin()); //Remove the first element because it is the current tile.
	nodePath.pop_back(); //Remove the last element because it is the target.
}

void GameScene::ResetNodes()
{
	for (int i = 0; i < TILE_LIST_SIZE; i++)
	{
		TacticsTile *tempTile = &tileList[i];
		tempTile->ResetNode();
		tempTile->SetDebugText(-1);
	}
}

void GameScene::SetHeuristicCost(TacticsTile *node)
{
	vector<TacticsTile *> listOfNodes;

	listOfNodes.push_back(node);

	for (unsigned i = 0; i < listOfNodes.size(); i++)
	{
		TacticsTile *tempNode = listOfNodes[i];

		TacticsTile *upNode = GetTileByGridPosition(tempNode->GetRow() - 1, tempNode->GetColl());
		TacticsTile *downNode = GetTileByGridPosition(tempNode->GetRow() + 1, tempNode->GetColl());
		TacticsTile *leftNode = GetTileByGridPosition(tempNode->GetRow(), tempNode->GetColl() - 1);
		TacticsTile *rightNode = GetTileByGridPosition(tempNode->GetRow(), tempNode->GetColl() + 1);

		if (upNode && upNode->Get_H_Cost() < 0)
			upNode->Set_H_Cost(tempNode->Get_H_Cost() + 1);

		if (downNode && downNode->Get_H_Cost() < 0)
			downNode->Set_H_Cost(tempNode->Get_H_Cost() + 1);

		if (leftNode && leftNode->Get_H_Cost() < 0)
			leftNode->Set_H_Cost(tempNode->Get_H_Cost() + 1);

		if (rightNode && rightNode->Get_H_Cost() < 0)
			rightNode->Set_H_Cost(tempNode->Get_H_Cost() + 1);

		if (CanAddNodeToList(upNode, &listOfNodes))
			listOfNodes.push_back(upNode);

		if (CanAddNodeToList(downNode, &listOfNodes))
			listOfNodes.push_back(downNode);

		if (CanAddNodeToList(leftNode, &listOfNodes))
			listOfNodes.push_back(leftNode);

		if (CanAddNodeToList(rightNode, &listOfNodes))
			listOfNodes.push_back(rightNode);
	}
}

TacticsTile* GameScene::MoveTile(TacticsTile* tile, int direction)
{
	int currentRow = tile->GetRow();
	int currentColl = tile->GetColl();

	switch (direction)
	{
		case MOVE_UP:
			if (currentRow - 1 >= 0 && grid[currentRow - 1][currentColl] == EMPTY_TILE)
			{
				SetMapValue(currentRow - 1, currentColl, tile->GetType());
				SetMapValue(currentRow, currentColl, EMPTY_TILE);

				tile = GetTileByGridPosition(currentRow - 1, currentColl);
			}
			break;

		case MOVE_DOWN:

			if (currentRow + 1 < GRID_SIZE_ROW && grid[currentRow + 1][currentColl] == EMPTY_TILE)
			{
				SetMapValue(currentRow + 1, currentColl, tile->GetType());
				SetMapValue(currentRow, currentColl, EMPTY_TILE);

				tile = GetTileByGridPosition(currentRow + 1, currentColl);
			}
			break;

		case MOVE_LEFT:
			if (currentColl - 1 >= 0 && grid[currentRow][currentColl - 1] == EMPTY_TILE)
			{
				SetMapValue(currentRow, currentColl - 1, tile->GetType());
				SetMapValue(currentRow, currentColl, EMPTY_TILE);

				tile = GetTileByGridPosition(currentRow, currentColl - 1);

			}
			break;

		case MOVE_RIGHT:

			if (currentColl + 1 < GRID_SIZE_COLL && grid[currentRow][currentColl + 1] == EMPTY_TILE)
			{
				SetMapValue(currentRow, currentColl + 1, tile->GetType());
				SetMapValue(currentRow, currentColl, EMPTY_TILE);

				tile = GetTileByGridPosition(currentRow, currentColl + 1);
			}

			break;

		default:
			break;
	}

	return tile;
}

