#include "HelloWorldScene.h"
#include "MenuScene.h"
#include "Definitions.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);


	
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();


	this->scheduleOnce(schedule_selector(HelloWorld::GoToMenuScene), DISPLAY_TIME_SPLASH_SCENE);

	

	auto backgroundSprite = Sprite::create("VFS.png");

	backgroundSprite->setPosition(Point(visibleSize.width * 0.5f + origin.x, visibleSize.height * 0.5f + origin.y));

	this->addChild(backgroundSprite);
	visibleSize;
    return true;
}

void HelloWorld::GoToMenuScene(float deltaTime)
{
	auto scene = MenuScene::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
}