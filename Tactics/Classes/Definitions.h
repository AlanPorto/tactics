#ifndef __DEFINITIONS_SCENE_H__
#define __DEFINITIONS_SCENE_H__

#define DISPLAY_TIME_SPLASH_SCENE 2
#define TIME_BETWEEN_TURNS 0.1
#define END_SCENE_DURATION 5
#define TRANSITION_TIME 0.5

#define GRID_SIZE_ROW 11
#define GRID_SIZE_COLL 15
#define TILE_LIST_SIZE (GRID_SIZE_ROW * GRID_SIZE_COLL)

// --- Directions
#define MOVE_UP 0
#define MOVE_DOWN 1
#define MOVE_LEFT 2
#define MOVE_RIGHT 3


// --- Tiles
#define EMPTY_TILE 0
#define WALL_TILE 1
#define KNIGHT_TILE 2
#define ARCHER_TILE 3
#define ASSASSIN_TILE 4
#define HEALER_TILE 5
#define TEAM_A_TILE 100
#define TEAM_B_TILE 110

// --- Teams
#define TEAM_SIZE 5
#define TEAM_A -1
#define TEAM_B -2


#endif // __DEFINITIONS_SCENE_H__
