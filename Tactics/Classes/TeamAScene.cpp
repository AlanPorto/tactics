#include "TeamAScene.h"
#include "MenuScene.h"
#include "Definitions.h"

USING_NS_CC;

Scene* TeamAScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
	auto layer = TeamAScene::create();

    // add layer as a child to scene
    scene->addChild(layer);


    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool TeamAScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	this->scheduleOnce(schedule_selector(TeamAScene::GoToMenuScene), END_SCENE_DURATION);

	auto backgroundSprite = Sprite::create("TeamA_won.png");

	backgroundSprite->setPosition(Point(visibleSize.width * 0.5f + origin.x, visibleSize.height * 0.5f + origin.y));

	this->addChild(backgroundSprite);
	visibleSize;
    return true;
}

void TeamAScene::GoToMenuScene(float deltaTime)
{
	auto scene = MenuScene::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
}