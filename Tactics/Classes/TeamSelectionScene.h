#ifndef __TEAM_SELECTION_SCENE_H__
#define __TEAM_SELECTION_SCENE_H__

#include "cocos2d.h"

class TeamSelectionScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // implement the "static create()" method manually
	CREATE_FUNC(TeamSelectionScene);
};

#endif // __TEAM_SELECTION_SCENE_H__
