#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"
#include "TacticsTile.h"
#include "BaseCharacter.h"

#include <vector>

#include <iostream>
using namespace std;

class GameScene : public cocos2d::Layer
{
public:
	// --- Variables

	// --- Methods
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // implement the "static create()" method manually
	CREATE_FUNC(GameScene);

	void update(float) override; //Overriding method
	

private:
	
	// --- Variables
	bool teamATurn;
	bool pathFound;

	float timer;
	int currentTurnIndex;
	int victoriousTeam;
	int grid[GRID_SIZE_ROW][GRID_SIZE_COLL];
	int teamA[TEAM_SIZE];
	int teamB[TEAM_SIZE];
	
	vector<TacticsTile*> nodePath;
	vector<BaseCharacter *> charactersList;


	BaseCharacter* currentChar;
	BaseCharacter* currentEnemy;
	TacticsTile* currentTile;
	TacticsTile* targetTile;
	EventListenerKeyboard *keyboardEvent;

	TacticsTile tileList[TILE_LIST_SIZE];

	// --- Methods
	TacticsTile* GetTileByGridPosition(int row, int col);
	
	bool IsGameOver();
	void MoveToEndScene();
	void CheckAttack();
	void ConstructGameScene();
	void DestroyGameScene();
	void NextTurn(float deltaTime);
	void CreateTeams();
	void CreateLevel();
	void SetMapValue(int row, int col, int value);

	void HandleKeyPress(EventKeyboard::KeyCode keyCode, Event* event);
	
	TacticsTile* MoveTile(TacticsTile* tile, int direction);

	void MoveToTarget();
	void ResetNodes();
	
	int GetDirectionToTile(TacticsTile *tile, TacticsTile *target);


	bool CanAddNodeToList(TacticsTile *node, vector<TacticsTile*>* checkList);
	void SetHeuristicCost(TacticsTile *node);
	void CreatePath(TacticsTile *endNode);
	bool StartPathFind(TacticsTile *startNode, TacticsTile *endNode);

	TacticsTile* GetNodeWithLowFCost(int fCost, vector<TacticsTile*>* checkList);
	BaseCharacter* GetCharacterByIndex(int team, int index);
	BaseCharacter* GetCharacterByPosition(int row, int coll);
};

#endif // __GAME_SCENE_H__

