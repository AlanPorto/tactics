#ifndef __BASE_CHARACTER_H__
#define __BASE_CHARACTER_H__

#include "Definitions.h"
#include "TacticsTile.h"

#include "cocos2d.h"
USING_NS_CC;
using namespace std;

class BaseCharacter
{
public:
	// --- Variables


	// --- Methods
	BaseCharacter(int teamValue, int typeValue);
	~BaseCharacter();

	void SetIsAlive(bool value);
	void SetPosition(int rowValue, int collValue);
	void SetRow(int value);
	void SetColl(int value);
	void SetIndex(int value);
	void SetNodePath(vector<TacticsTile *>* path);
	void StartTurn();


	bool GetIsAlive();
	int GetRow();
	int GetColl();
	int GetIndex();
	int GetTeam();

	TacticsTile* GetTileToMove();
	BaseCharacter* SearchForEnemy(vector<BaseCharacter *> *charactersList);

private:
	// --- Variables
	bool isAlive;
	int maxMove;
	int moveCount;
	int team;
	int type;
	int myIndex;
	int row;
	int coll;

	vector<TacticsTile *>* nodePath;

	// --- Methods
	void SetMovementByType(int typeValue);
	
protected:


};

#endif // __BASE_CHARACTER_H__

