#ifndef __TEAM_A_SCENE_H__
#define __TEAM_A_SCENE_H__

#include "cocos2d.h"

class TeamAScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // implement the "static create()" method manually
	CREATE_FUNC(TeamAScene);

private:
	void GoToMenuScene( float deltaTime);

};

#endif // __TEAM_A_SCENE_H__
